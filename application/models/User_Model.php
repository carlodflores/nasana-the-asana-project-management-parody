<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model {

	function __construct(){
	parent::__construct();
	}

	function get_user_by_username($username) {
		$this->db->select('*');
		$this->db->from('nsn_users');
		$this->db->where('username', $username);
		$query = $this->db->get();
		return $query->row();
	}

	function get_user_by_id($id) {
		$this->db->select('*');
		$this->db->from('nsn_users');
		$this->db->where('user_id', $id);
		$query = $this->db->get();
		return $query->row();
	}

	function get_all_users() {
		return $this->db->get('nsn_users')->result();
	}

	function add_user($data) {
		$this->db->insert('nsn_users', $data);
		$check = $this->db->insert_id();
		return $check;
	}
}
