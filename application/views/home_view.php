<body data-page-title="login">
    <nav>
        <div class="sixteen colgrid">
            <div class="row">
                <div class="four columns">
                    <img src="<?php echo base_url() ?>img/logo.png" alt="" />
                </div>

                <div class="nine columns"></div>

                <div class="three columns" style="padding-top: 3px;">
                    <button class="switch active btn-blue" gumby-trigger="#login-panel">LOGIN</button>
                </div>
            </div>
        </div>
    </nav>

    <di class="sixteen colgrid">
        <br/><br/>
        <div class="row">
            <div class="centered eight columns">
                <center>
                    <h1>A Collaboration Tool for Students</h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </center>
            </div>
        </div>
    </di>

    <div class="modal" id="login-panel">
        <div class="content">
            <a class="close switch" gumby-trigger="|#login-panel"><i class="icon-cancel" /></i></a>
            <div class="row">
                <div class="centered six columns">
                    <center>
                        <form action="<?php echo base_url() ?>login/" method="post">
                            <span class="module-title">LOGIN</span><br/><br/>
                            <label for="loginusername" class="input-label">Email Address</label>
                            <span class="field"><input class="text input xxwide" type="text" name="loginusername" placeholder="name@company.com"></span>
                            <?php echo form_error('loginusername', '<span class="frm-error">', '</span>'); ?>
                            <br/><br/>
                            <label for="loginpassword" class="input-label">Password</label>
                            <span class="field"><input class="text input xxwide" type="password" name="loginpassword" placeholder="Password"></span>
                            <?php echo form_error('loginpassword', '<span class="frm-error">', '</span>'); ?>
                            <br/><br/>
                            <span><input type="submit" class="btn-frm-submit" value="LOGIN" /></span>
                        </form>
                    </center>
                </div>
            </div>
        </div>
    </div>
</body>
