<body data-page-title="login">
    <div class="sixteen colgrid">
        <div class="row">
            <div class="centered five columns login-holder">
                <center>
                    <img src="<?php echo base_url() ?>img/logo.png" alt="" />
                    <form action="<?php echo base_url() ?>login/" method="post">
                        <span class="module-title">LOGIN</span><br/><br/>
                        <label for="loginusername" class="input-label">Email Address</label>
                        <span class="field"><input class="text input xxwide" type="text" name="loginusername" placeholder="name@company.com"></span>
                        <?php echo form_error('loginusername', '<span class="frm-error">', '</span>'); ?>
                        <br/><br/>
                        <label for="loginpassword" class="input-label">Password</label>
                        <span class="field"><input class="text input xxwide" type="password" name="loginpassword" placeholder="Password"></span>
                        <?php echo form_error('loginpassword', '<span class="frm-error">', '</span>'); ?>
                        <br/><br/>
                        <span><input type="submit" class="btn-frm-submit" value="LOGIN" /></span>
                    </form>
                </center>
            </div>
        </div>
    </div>
</body>
