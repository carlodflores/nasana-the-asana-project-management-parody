<h2 style="color: #fff;"><?php echo $task->task_title ?></h2>
<span style="font-size: 10px; margin-top: -10px;">
    <ul>
        <li style="padding-left: 20px; margin: -15px; !important"><i class="icon-user"> </i><?php echo $this->User_Model->get_user_by_id($task->task_added)->display_name ?></li>
        <li style="padding-left: 20px; margin: -15px; !important"><i class="icon-code"> </i><?php echo $this->User_Model->get_user_by_id($task->task_assign)->display_name ?></li>
        <li style="padding-left: 20px; margin: -15px; !important"><i class="icon-tag"> </i><?php echo $this->Task_Model->get_section_data_id($task->section_id)->section_name ?></li>
        <li style="padding-left: 20px; margin: -15px; !important"><?php echo ($task->task_status == 0)? '<i class="icon-thumbs-up"> </i>'. $task->task_done: '<i class="icon-thumbs-down"> </i>Working on it'; ?></li>
        <li style="padding-left: 20px; margin: -15px; !important"><i class="icon-calendar"> </i><?php echo $task->date_created ?></li>
        <li style="padding-left: 20px; margin: -15px; !important"><i class="icon-alert"> </i><?php switch ($task->task_priority) {
case '1': echo 'Low'; break;
case '2': echo 'Mid'; break;
case '3': echo 'High'; break;
        } ?></li>
    </ul>
</span><Br/>
<span class="module-title">Description</span>
<p>
    <?php echo $task->task_desc ?>
</p>
