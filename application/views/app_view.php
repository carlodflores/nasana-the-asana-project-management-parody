<?php  $session = $this->session->userdata('admin_logged_in');  ?>
<body data-page-title="login">
    <nav>
        <div class="sixteen colgrid">
            <div class="row">
                <div class="four columns">
                    <img src="<?php echo base_url() ?>img/logo.png" alt="" />
                </div>

                <div class="nine columns" style="padding-top: 5px;">
                    <a class="switch active btn-blue-bolder" gumby-trigger="#add-task-panel">Add Task</a> ·
                    <a class="switch active btn-blue-bolder" gumby-trigger="#assigned-task-panel">Task Assigned</a> ·
                    <a class="switch active btn-blue-bolder" gumby-trigger="#productivity-task-panel">Productivity</a> ·
                    <a class="switch active btn-blue-bolder" gumby-trigger="#user_panel">Team</a>
                    <?php echo($session['user_id'] == 1)? ' · <a class="switch active btn-blue-bolder" gumby-trigger="#add_user_panel">Add User</a>' :''; ?>
                </div>

                <div class="three columns" style="padding-top: 5px;">
                    <span><a  class="switch active btn-blue-bolder" href="<?php echo base_url() ?>login/logout/"><?php echo $info['display_name'] ?></a>  · </span>
                    <span><a  class="btn-blue-bolder" href="<?php echo base_url() ?>login/logout/">Sign Out</a></span>
                </div>
            </div>
        </div>
    </nav>

    <style media="screen">
    div::-webkit-scrollbar {
        width: 5px;
    }

    div::-webkit-scrollbar-track {
        opacity: 0;
    }

    div::-webkit-scrollbar-thumb {
        border-radius: 3px;
        background: rgba(0, 0, 0, 0.5);
    }
    </style>

    <div class="sixteen colgrid">
        <div class="row">
            <br/><br/>
            <div class="nine columns">
                <div class="task-container" style="max-height: calc(100vh - 200px); position: fixed; width: 118vh; left: 0; padding: 0 0 0 5%; overflow-y: auto;">
                    <?php if(isset($error_message)) { ?>
                        <ul> <li class="danger alert"><?php echo $error_message ?></li> </ul>
                    <?php } ?>

                    <?php foreach ($sections as $key => $value) { ?>
                        <span class="module-title"><?php echo $value->section_name ?></span> <i class="section-edit icon-pencil" data-id="<?php echo $value->section_id ?>"   data-value="<?php echo $value->section_name ?>"> </i><i class="icon-cancel section-delete"  data-id="<?php echo $value->section_id ?>"   data-value="<?php echo $value->section_name ?>">  </i>
                        <?php
                            $tasks = $this->Task_Model->get_all_task_data($value->section_id);
                            foreach ($tasks as $key => $task) {
                         ?>
                        <div class="task-list-holder" style="margin-bottom: -20px;">
                            <ul>
                                <li class="task-list"  data-id="<?php echo $task->task_id ?>">
                                    <span style="float: left; margin-right: 10px;"><div class="done-btn <?php echo ($task->task_status == 0)? 'done' : ''; ?>" data-id="<?php echo $task->task_id ?>"><i class="icon-check" style="margin: 5px 0 0 3px;"></i></div></span>
                                    <span class="task-title-trig" data-id="<?php echo $task->task_id ?>"><?php echo $task->task_title ?></span><span style="opacity: 0.6;"> <i  data-id="<?php echo $task->task_id ?>" class="task-edit icon-pencil" data-value="<?php echo $task->task_id ?>"> </i>
                                        <i class="task-delete icon-cancel" data-id="<?php echo $task->task_id ?>">  </i></span>
                                    <span style="float: right;"><i class="icon-user"> </i><?php echo ($task->task_assign != $session['user_id'])? $this->User_Model->get_user_by_id($task->task_assign)->display_name : "Me"; ?></span>
                                </li>
                            </ul>
                        </div>


                    <?php } echo '<br/><br/>'; } ?>
                </div>
                <div style="max-height: calc(100vh - 200px); position: fixed; max-width: 100vh; width: 100vh; bottom: 10px; left: 0; padding: 0 0 0 5%; overflow-y: auto; font:700 12px 'Open Sans', sans-serif;">
                    CSQUARED DEV © nasana 2015
                </div>
            </div>

            <div class="six columns">
                <div class="task-info-panel" id="task_loader" style="width: 85.5vh; height: calc(100% - 70px); right: 0; bottom: 0;">
                    <h2 style="color: #fff;">Project Status</h2><span style="float:right;" class="module-title chart_refresh">Refresh</span><Br/>
                    <span style="background: rgba(62, 150,211, 0.80); padding: 10px;"> Work Done</span>
                    <span style="background: rgba(48, 214, 198, .60); padding: 10px;"> Work Remaining</span><Br/><Br/>
                    <canvas id="chart" style="max-width: 100%; max-height: 300px !important;"></canvas>
                    <small>    * Not a real time chart. Needs refresh for updated data.</small>
                </div>

            </div>
        </div>
    </div>

    <div class="modal" id="productivity-task-panel">
        <div class="content" style="background: #203b53; color: #fff;">
            <a class="close switch" gumby-trigger="|#productivity-task-panel"><i class="icon-cancel" /></i></a>
            <div class="row">
                <div class="centered twelve columns">
                    <span class="module-title">Project Status</span><span style="float:right;" class="module-title chart_refresh">Refresh</span><br/><br/>
                    <span style="background: rgba(62, 150,211, 0.80); padding: 10px;"> Work Done</span>
                    <span style="background: rgba(48, 214, 198, .60); padding: 10px;"> Work Remaining</span><Br/><Br/>
                    <canvas id="chart2" style="max-width: 100%;"></canvas>
                    <small>    * Not a real time chart. Needs refresh for updated data.</small>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="user_panel">
        <div class="content">
            <a class="close switch" gumby-trigger="|#user_panel"><i class="icon-cancel" /></i></a>
            <div class="row">
                <div class="centered twelve columns">
                    <table class="rounded">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Display Name</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                                foreach ($users as $key => $value) {
                                    echo "<tr><td style='font-size: 14px;'>". $value->username ."</td><td style='font-size: 14px;'>". $value->display_name ."</td></tr>";
                                }
                             ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="add_user_panel">
        <div class="content">
            <a class="close switch" gumby-trigger="|#add_user_panel"><i class="icon-cancel" /></i></a>
            <div class="row">
                <div class="centered twelve columns">
                    <span class="module-title">Add User</span><br/>
                    <form action="<?php echo base_url() ?>app/add_user/" method="post">
                        <div class="centered six columns">
                            <center>
                                <label for="task_title" class="input-label">Email</label>
                                <span class="field"><input class="text input xxwide" type="text" name="email" placeholder="Email Address"></span>
                                <?php echo form_error('task_title', '<span class="frm-error">', '</span>'); ?>
                                <br/>
                                <label for="task_title" class="input-label">Display Name</label>
                                <span class="field"><input class="text input xxwide" type="text" name="display" placeholder="Display Name"></span>
                                <?php echo form_error('task_title', '<span class="frm-error">', '</span>'); ?>
                                <br/>
                                <label for="task_title" class="input-label">Password</label>
                                <span class="field"><input class="text input xxwide" type="password" name="password" placeholder="Password"></span>
                                <?php echo form_error('task_title', '<span class="frm-error">', '</span>'); ?>
                                <br/><br/>
                                <span><input type="submit" class="btn-frm-submit" value="Add User" /></span>
                            </center>
                        </div>
                    </form>
                </div>
            </diV>
        </div>
    </div>


    <div class="modal" id="assigned-task-panel">
        <div class="content">
            <a class="close switch" gumby-trigger="|#assigned-task-panel"><i class="icon-cancel" /></i></a>
            <div class="row">
                <div class="centered twelve columns">
                    <span class="module-title">Task Assigned to You (<?php echo count($assigned) ?>)</span><br/>
                    <?php
                        foreach ($assigned as $key => $value) {
                    ?>

                    <div class="task-list-holder">
                        <ul>
                            <li class="task-list close switch" gumby-trigger="|#assigned-task-panel" data-id="<?php echo $value->task_id ?>">
                                <span style="float: left; margin-right: 10px;"><div class="done-btn <?php echo ($value->task_status == 0)? 'done' : ''; ?>" data-id="<?php echo $value->task_id ?>"><i class="icon-check" style="margin: 5px 0 0 3px;"></i></div></span>
                                <?php echo $value->task_title ?>
                                <span style="float: right;"><i class="icon-user"> </i><?php echo $this->User_Model->get_user_by_id($value->task_assign)->display_name ?></span>
                            </li>
                        </ul>
                    </div>

                    <?php
                        }
                     ?>
                </div>
            </diV>
        </div>
    </div>

    <div class="modal" id="add-task-panel">
        <div class="content">
            <a class="close switch" gumby-trigger="|#add-task-panel"><i class="icon-cancel" /></i></a>
            <div class="row">
                <div class="centered twelve columns">

                    <form action="<?php echo base_url() ?>app/add_task/" method="post">
                        <span class="module-title">Add Task</span><br/><br/>
                        <center>
                            <label for="task_title" class="input-label">Task Title</label>
                            <span class="field"><input class="text input xxwide" type="text" name="task_title" placeholder="Task Title"></span>
                            <?php echo form_error('task_title', '<span class="frm-error">', '</span>'); ?>
                            <br/><br/>

                            <label for="task_description" class="input-label">Task Description</label>
                            <span class="field"><textarea class="textarea input xxwide" name="task_description" style="font-size: 12px;" placeholder="Task Description"></textarea></span>
                            <?php echo form_error('task_description', '<span class="frm-error">', '</span>'); ?>
                            <br/><br/>

                            <div class="row">
                                <div class="four columns">
                                    <label for="task_priority" class="input-label">Task Priority</label>
                                    <span class="field">
                                        <select class="select input xxwide" name="task_priority">
                                            <option value="#" disabled>Task Priority</option>
                                            <option value="1">Low Priority</option>
                                            <option value="2">Med Priority</option>
                                            <option value="3">High Priority</option>
                                        </select>
                                    </span><br/>
                                    <?php echo form_error('task_priority', '<span class="frm-error">', '</span>'); ?>
                                </div>
                                <div class="four columns">
                                    <label for="task_section" class="input-label">Task Section</label>
                                    <span class="field">
                                        <input class="text input xxwide" type="text" name="task_section" placeholder="Task Section" list="task_sections">
                                        <datalist id="task_sections">
                                            <?php
                                                foreach ($sections as $key => $value) {
                                                    echo "<option value='". $value->section_name ."'>";
                                                }
                                            ?>
                                        </datalist>
                                    </span><br/>
                                    <?php echo form_error('task_section', '<span class="frm-error">', '</span>'); ?>
                                </div>
                                <div class="four columns">
                                    <label for="task_assign" class="input-label">Assign to</label>
                                    <span class="field">
                                        <input class="text input xxwide" type="text" name="task_assign" placeholder="Team Member" list="team_member">
                                        <datalist id="team_member">
                                            <?php
                                                foreach ($users as $key => $value) {
                                                    echo "<option value='". $value->username ."'>";
                                                }
                                            ?>
                                        </datalist>
                                    </span><br/>
                                    <?php echo form_error('task_assign', '<span class="frm-error">', '</span>'); ?>
                                </div>
                            </div>
                            <?php echo form_error('task_assign', '<span class="frm-error">', '</span>'); ?>
                            <br/>
                            <span><input type="submit" class="btn-frm-submit" value="Add Task" /></span>
                        </center>
                    </form>

                </div>
            </div>
        </div>
    </div>
</body>

<div id="alert-handler" style="position: absolute; z-index: 99999; right: 0; bottom: 0; padding: 10px;">

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script>
$(function() {
    Chart.defaults.global.animationEasing        = 'easeInOutQuad',
    Chart.defaults.global.responsive             = true;
    Chart.defaults.global.scaleOverride          = true;
    Chart.defaults.global.scaleShowLabels        = false;
    Chart.defaults.global.scaleSteps             = 10;
    Chart.defaults.global.scaleStepWidth         = 1;
    Chart.defaults.global.scaleStartValue        = 0;
    Chart.defaults.global.tooltipFontFamily      = 'Open Sans';
    Chart.defaults.global.tooltipFillColor       = 'rgba(0, 0, 0, 0.5)';
    Chart.defaults.global.tooltipFontColor       = '#fff';
    Chart.defaults.global.tooltipCaretSize       = 0;
    Chart.defaults.global.maintainAspectRatio    = true;

    Chart.defaults.Line.scaleShowHorizontalLines = false;
    Chart.defaults.Line.scaleShowHorizontalLines = false;
    Chart.defaults.Line.scaleGridLineColor       = 'transparent';
    Chart.defaults.Line.scaleLineColor           = '#24d5aa';

    function refresh() {

        $.post('<?php echo base_url() ?>app/get_progress/', function(e){


            var chart    = document.getElementById('chart').getContext('2d'),
             chart2    = document.getElementById('chart2').getContext('2d'),
                gradient = chart.createLinearGradient(0, 0, 0, 450);

                var ex = $.parseJSON(e);
                var data  = {
                    labels: ex.labels,
                    datasets: [

                        {
                          label: 'Remaining',
                          fillColor: 'rgba(62, 150,211, 0.80)',
                          strokeColor: '#3f94d4',
                          pointColor: '#3f94d4',
                          pointStrokeColor: 'rgba(36,213,171,1)',
                          pointHighlightStroke: 'rgba(36,213,171,1)',
                          data: ex.remain
                      },
                        {
                          label: 'Done',
                          fillColor: gradient,
                          strokeColor: '#24d5aa',
                          pointColor: '#24d5aa',
                          pointStrokeColor: 'rgba(36,213,171,1)',
                          pointHighlightStroke: 'rgba(36,213,171,1)',
                          data: ex.done
                      }
                    ]
                };

                gradient.addColorStop(0, 'rgba(62, 150,211, 0.80)');
                gradient.addColorStop(0.5, 'rgba(48, 214, 198, .60)');
                gradient.addColorStop(1, 'rgba(36, 213, 171, .30)');

                var chart = new Chart(chart).Line(data);
                var chart2 = new Chart(chart2).Line(data);
            });

    }
    refresh();

    $(".chart_refresh").click(function() {
        refresh();
    });
});


</script>
<script>
    $(function() {



        if(window.location.hash) {
            var data = window.location.hash,
                data = data.split('/');

            if(data.length == 2) {
                var id = data[1];
                $("#task_loader").load("<?php echo base_url() ?>app/task_information/" + id);
            } else if(data.length == 4 && data[1] == "task") {
                var id = data[3];
                $("#task_loader").load("<?php echo base_url() ?>app/edit_task_info/" + id);
            }
        }

        $(".task-title-trig").click(function() {
            var id = $(this).attr('data-id');
            location.hash = '/' + id;
            $("#task_loader").load("<?php echo base_url() ?>app/task_information/" + id);
        });

        $(".task-edit").click(function() {
            var id = $(this).attr('data-id');
            location.hash = '/task/edit/' + id;
            $("#task_loader").load("<?php echo base_url() ?>app/edit_task_info/" + id);
        });

        $(".task-delete").click(function() {
            var id = $(this).attr('data-id');
            var c = confirm("Are you sure you want to delete this task?");
            if(c) {
                $.post('<?php echo base_url() ?>app/delete_task/'+id, function(data) {
                    location.hash = '/delete/task/' + id;
                    location.reload();
                });
            }

        });

        $(".section-edit").click(function() {
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-value');

            var c = prompt("Enter new section name", ""+name);
            $.post("<?php echo base_url() ?>app/update_section/"+id, {c:c}, function(data) {
                location.reload();
            });
        });

        $(".section-delete").click(function() {
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-value');
            var c = confirm("Are you sure you want to delete "+ name +" together with the task under it?");
            if(c) {
                $.post("<?php echo base_url() ?>app/delete_section/"+id, function(data) {
                    location.reload();
                });
            }
        });

        $(".done-btn").click(function() {
            var id = $(this).attr('data-id');
            var index = $(this).index('.done-btn');
            $.post('<?php echo base_url() ?>app/update_task/'+id, function(data) {
                if(data == "true/push") {
                    $("#alert-handler").append('<div class="alert-content primary alert">Thank you for being productive.</div>').delay(700).fadeOut(function() { $(this).remove(); });
                    $(".done-btn:eq(" + index + ")" ).addClass("done");
                    $(".task-list:eq(" + index + ")" ).attr('style', 'opacity: 0.3 !imporant;').fadeOut("fast").fadeIn("fast").css({opacity:0.3 + "!important"});
                } else if(data == "true/revoke") {
                    $("#alert-handler").append('<div class="alert-content warning alert">Done Status has been revoked.</div>').delay(700).fadeOut(function() { $(this).remove(); });
                    $(".done-btn:eq(" + index + ")" ).removeClass("done");
                    $(".task-list:eq(" + index + ")" ).removeAttr('style').fadeOut("fast").fadeIn("fast").css({opacity:1 + "!important"});
                } else {
                    $("#alert-handler").append('<div class="alert-content danger alert">Something went wrong.</div>').delay(700).fadeOut(function() { $(this).remove(); });
                    $(".done-btn:eq(" + index + ")" ).addClass("failed");
                    $(".task-list:eq(" + index + ")" ).removeAttr('style').fadeOut("fast").fadeIn("fast").css({opacity:1 + "!important"});
                }
            });
        });
    });
</script>
