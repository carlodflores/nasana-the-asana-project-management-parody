<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
	parent::__construct();

		$this->load->model('User_Model');
		$this->load->library('parser');
	}

	public function index() {
		if($this->session->userdata('admin_logged_in')){
			redirect(base_url() .'app/');
			exit;
		}

		$data['page_title'] = "nasana - Collaboration Tool";
		$this->parser->parse('header_view', $data);
		$this->parser->parse('home_view', $data);
		$this->parser->parse('footer_view', $data);
	}
}
