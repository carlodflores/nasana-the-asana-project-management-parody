<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
	parent::__construct();

		$this->load->model('User_Model');
		$this->load->library('parser');
	}

	public function index() {
		if($this->session->userdata('admin_logged_in')){
			redirect(base_url() .'app/');
			exit;
		}

		$this->form_validation->set_rules('loginusername', 'Username ', 'required|xss_clean|callback_loginpassword_check');
		$this->form_validation->set_rules('loginpassword', 'Password ', 'required|xss_clean');
		if($this->form_validation->run() == true) {
			redirect(base_url() .'app/');
		}

		$data['page_title'] = "nasana - LOGIN";
		$this->parser->parse('header_view', $data);
		$this->parser->parse('login_view', $data);
		$this->parser->parse('footer_view', $data);
	}

	public function loginpassword_check($login_username) {
		$result = $this->User_Model->get_user_by_username($login_username);
		if($result) {
			if($result->status == 1) {
				if(md5($this->input->post('loginpassword')) == $result->password) {
					$admin_info = array(
						'user_id' => $result->user_id,
						'username' => $result->username,
						'display_name' => $result->display_name
					);
					$this->session->set_userdata('admin_logged_in', $admin_info);
					return true;
				} else {
					$this->form_validation->set_message('loginpassword_check', 'Wrong Username and Password combination.');
					return false;
				}
			} else {
				$this->form_validation->set_message('loginpassword_check', 'Username is deactivated.');
				return false;
			}
		} else {
			$this->form_validation->set_message('loginpassword_check', 'Username does not exists');
			return false;
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
