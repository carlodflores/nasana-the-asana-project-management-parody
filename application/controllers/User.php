<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
	parent::__construct();

		$this->load->model('User_Model');
		$this->load->library('parser');
	}

	public function index() {
		redirect(base_url() .'user/register/');
	}

	public function register() {
		if($this->session->userdata('admin_logged_in')){
			redirect(base_url() .'app/');
			exit;
		}

		$data['page_title'] = "nasana - Collaboration Tool";
		$this->parser->parse('header_view', $data);
		$this->parser->parse('register_view', $data);
		$this->parser->parse('footer_view', $data);
	}
}
