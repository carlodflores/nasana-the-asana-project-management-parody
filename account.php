<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en" itemscope itemtype="http://schema.org/Product"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/b/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Hair Geek - Products</title>
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta name="author" content="humans.txt">

  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />

  <!-- Facebook Metadata /-->
  <meta property="fb:page_id" content="" />
  <meta property="og:image" content="" />
  <meta property="og:description" content=""/>
  <meta property="og:title" content=""/>

  <!-- Google+ Metadata /-->
  <meta itemprop="name" content="">
  <meta itemprop="description" content="">
  <meta itemprop="image" content="">

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

  <!-- We highly recommend you use SASS and write your custom styles in sass/_custom.scss.
       However, there is a blank style.css in the css directory should you prefer -->
  <link rel="stylesheet" href="css/gumby.css">
  <script src="js/libs/modernizr-2.6.2.min.js"></script>
</head>

<body>

    <nav>
        <div class="sixteen colgrid">
    		<div class="row">
                <div id="wide-menu">
                    <div class="three columns">
                        <img src="img/top-logo.jpg" alt="">
                    </div>

                    <div class="eight columns">
                        <ul class="main-menu">
                            <li class="main-menu-items">Products</li>
                            <li class="main-menu-items">Inquries</li>
                            <li class="main-menu-items">Feedback</li>
                            <li class="main-menu-items">Sign In</li>
                        </ul>
                    </div>

                    <div class="five columns">
                    <div id="nav-items-holder" style="float:right;"><i class="icon-search"></i><span class="nav-items"> Search</span> <i class="icon-basket"></i><span class="nav-items"> Cart</span></div>
                    </div>
                </div>

                <div id="mobile-menu">
                    <ul id="mobile-menu-container">
                        <li class="menus"><i id="mobile-menu-trigger" class="icon-menu nav-mobile-items mobile-menu"></i></li>
                        <li class="menus"><img src="img/top-logo.jpg" alt=""></li>
                        <li class="menus"> <i class="icon-basket nav-mobile-items mobile-cart"></i></li>
                    </ul>

                    <div class="sixteen colgrid">
                        <div class="row">
                            <div class="mobile-menu-holder">
                                <br/><br/>
                                <ul id="main-mobile-menu">
                                    <li class="main-mobile-menu-items menu-extend">
                                        Products
                                        <span class="main-mobile-menu-extend" style="float:right;">+</span>
                                        <ul style="display:none;">
                                            <li>All Products</li>
                                            <li>Hair Dye</li>
                                            <li>Hair Treatment</li>
                                            <li>Hair Tools</li>
                                        </ul>
                                    </li>
                                    <li class="main-mobile-menu-items">Inquries</li>
                                    <li class="main-mobile-menu-items">Feedback</li>
                                    <li class="main-mobile-menu-items">Sign In</li>
                                    <li class="main-mobile-menu-items">
                                        <center>
                                            <input type="text" class="medium" placeholder="Search our products" />
                                            <input type="submit" class="medium add-pad" style="margin-left: 0px;" name="footer-submit" value="Search">
                                        </center>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

	<div class="sixteen colgrid">
        <br/><br/>
        <div class="row">

            <div class="push_two six columns account-holder">
                <center>
                    <span class="module-title">sign in</span>
                    <p class="small-info">For registered/returning customers<br/><br/> </p>
                    <form method="post">
                        E-mail
                        <input type="text" class="field xxwide" placeholder="Email" /><Br/><Br/>
                        Password
                        <input type="password" class="field xxwide"  placeholder="Password" /><br/><br/>
                        <input type="submit" class="narrow" style="margin-left: 0px;" name="footer-submit" value="Sign In">
                    </form>
                </center>
            </div>

            <div class="six columns account-holder">
                <center>
                    <span class="module-title">sign up</span>
                    <p class="small-info">Create account to quicken future checkout,<br/>receive discounts and special offers.</p>
                    <form method="post">
                        E-mail
                        <input type="text" class="field xxwide" placeholder="Email" /><Br/><Br/>
                        Password
                        <input type="password" class="field xxwide"  placeholder="Password" /><br/><br/>
                        Confirm Password
                        <input type="password" class="field xxwide"  placeholder="Confirm Password" /><br/><br/>
                        <p class="small-info">
                            By clicking the sign up you’ve aggreed to<br/>
receive a promotions, and special offers.
                        </p><br/>
                        <input type="submit" class="narrow" style="margin-left: 0px;" name="footer-submit" value="Sign Up">
                    </form>
                </center>
            </div>

        </div>
        <br/><br/>
	</div>

    <footer>
        <div class="sixteen colgrids">
            <div class="row">
                <hr class="black-divider" />

                <div class="centered twelve columns">
                    <ul id="footer-container">
                        <li class="contents">
                            <span class="footer-title">Important Links</span>
                            <ul id="footer-links">
                                <li>Order Status</li>
                                <li>Payment</li>
                                <li>Returns</li>
                                <li>Contact Us</li>
                            </ul>
                        </li>

                        <li class="contents">
                            <span class="footer-title">Subscribe</span>
                            <form action="" method="post">
                                <input type="text" class="xwide footer-field" name="footer-email" placeholder="Email" />
                                <input type="text" class="xwide footer-field" name="footer-fname" placeholder="Full Name" />
                                <input type="submit" class="xwide footer-field" style="margin-left: 0px;" name="footer-submit" value="Submit">
                            </form>
                        </li>

                        <li class="contents">
                            <span class="footer-title">Folow Us On</span><Br/>
                            <i class="icon-facebook"></i>
                            <i class="icon-twitter"></i>
                            <i class="icon-instagram"></i>
                            <i class="icon-gplus"></i>

                            <Br/><Br/>
                            <span class="footer-title">Powered By: CSQUARED Dev</span><Br/>
                        </li>

                        <li class="contents">
                            <span class="footer-title">Quick Info</span>
                            <ul id="footer-links">
                                <li><span><i class="icon-address"></i></span>143 Sample st. Sample City 1610</li>
                                <li><span><i class="icon-mobile"></i></span>(+63) 0912-1234-123</li>
                                <li><span><i class="icon-phone"></i></span>(02) 6464-646</li>
                                <li><span><i class="icon-mail"></i></span>support@hairgeek.ph</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
  <!-- Grab Google CDN's jQuery, fall back to local if offline -->
  <!-- 2.0 for modern browsers, 1.10 for .oldie -->
  <script>
  var oldieCheck = Boolean(document.getElementsByTagName('html')[0].className.match(/\soldie\s/g));
  if(!oldieCheck) {
    document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"><\/script>');
  } else {
    document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"><\/script>');
  }
  </script>
  <script>
  if(!window.jQuery) {
    if(!oldieCheck) {
      document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');
    } else {
      document.write('<script src="js/libs/jquery-1.10.1.min.js"><\/script>');
    }
  }
  </script>
 <script>
    $(function() {
        $("#mobile-menu-trigger").click(function(){
            $("#main-mobile-menu").slideToggle("fast");
        });

        $("div.product-img-holder").hover(function() {
            $(".qview-" + $(this).attr("data-value")).slideToggle("fast");
        });

        $(".menu-extend").click(function() {
            $(this).find('ul').slideToggle("fast");
        });

        $("input[name='quick-add-product']").click(function() {
            $('.alert-lower-left').fadeIn('fast').delay(1000).fadeOut('slow');
        });
    });
 </script>

  <!--
  Include gumby.js followed by UI modules followed by gumby.init.js
  Or concatenate and minify into a single file -->
  <script gumby-touch="js/libs" src="js/libs/gumby.js"></script>
  <script src="js/libs/ui/gumby.retina.js"></script>
  <script src="js/libs/ui/gumby.fixed.js"></script>
  <script src="js/libs/ui/gumby.skiplink.js"></script>
  <script src="js/libs/ui/gumby.toggleswitch.js"></script>
  <script src="js/libs/ui/gumby.checkbox.js"></script>
  <script src="js/libs/ui/gumby.radiobtn.js"></script>
  <script src="js/libs/ui/gumby.tabs.js"></script>
  <script src="js/libs/ui/gumby.navbar.js"></script>
  <script src="js/libs/ui/jquery.validation.js"></script>
  <script src="js/libs/gumby.init.js"></script>

  <!--
  Google's recommended deferred loading of JS
  gumby.min.js contains gumby.js, all UI modules and gumby.init.js
  <script type="text/javascript">
  function downloadJSAtOnload() {
  var element = document.createElement("script");
  element.src = "js/libs/gumby.min.js";
  document.body.appendChild(element);
  }
  if (window.addEventListener)
  window.addEventListener("load", downloadJSAtOnload, false);
  else if (window.attachEvent)
  window.attachEvent("onload", downloadJSAtOnload);
  else window.onload = downloadJSAtOnload;
  </script> -->

  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>

  <!-- Change UA-XXXXX-X to be your site's ID -->
  <!--<script>
  window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
  Modernizr.load({
    load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
  });
  </script>-->

  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
     chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7 ]>
  <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->

  </body>
</html>
